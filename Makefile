all:
	 unzip libMR.zip
	 sed -i.bkp "s@LIBMR_DIR = .*@LIBMR_DIR = $(shell pwd)/libMR@g" libsvm-openset/Makefile
	 $(MAKE) -C libsvm-openset
	 unzip SML4DGA.zip DGA/Data_Duval/Duval_Classification_1.mat
	 ln -s DGA/Data_Duval/Duval_Classification_1.mat dga_data
	 unzip UCI\ HAR\ Dataset.zip UCI\ HAR\ Dataset/t*/[Xy]*
	 mv UCI\ HAR\ Dataset/* uci_har_data
	 rmdir UCI\ HAR\ Dataset
	 mkdir lbp88_data
	 tar xf lbp-like-feature-vectors-subset-88.tar.gz -C lbp88_data

clean:
	rm -r lbp88_data dga_data DGA uci_har_data/t* libMR
	$(MAKE) clean -C libsvm-openset
	pyclean .
