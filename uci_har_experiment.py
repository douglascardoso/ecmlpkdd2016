#BitStringEncoder: all_bits
import cPickle
import collections as cl
import functools as ft
import gzip
import itertools as it
import pdb
import signal
import sys
import time

import numpy as np
np.set_printoptions(4, linewidth=9999, suppress=1)
import scipy.io as io
import scipy.stats as st
import sklearn.cross_validation as cv
import sklearn.metrics as metrics
import sklearn.naive_bayes as nb
import sklearn.neighbors as neighbors
import sklearn.preprocessing as preprocessing
import sklearn.svm as svm

import wann.encoding as encoding
import wann.wisard.classifiers as wis_clf
import wann.util as util

import my_libsvm

run_tag = 'uci_har_%d'%time.time()

fbeta = lambda x: ft.partial(metrics.fbeta_score, beta=x)

def compare_alternatives(train_data, test_data, train_labels, left_out):
    results = []
    addresser = encoding.EncodingComposer(
        encoding.UnaryEncoder(20), encoding.BitStringEncoder(50, 200))

    clf_alternatives = [
        svm.SVC(1000),
        neighbors.KNeighborsClassifier(),
        nb.GaussianNB(),
        wis_clf.OmniDiscriminator(addresser),
        wis_clf.RejectionOmniDiscriminator(addresser, fbeta(2.5), left_out),
        wis_clf.RejectionOmniDiscriminator(addresser, 10, left_out),
        ]

    for clf in clf_alternatives:
        clf.fit(train_data, train_labels)
        results.append(clf.predict(test_data))

    results.append(my_libsvm.do_svm(
        train_data, train_labels, test_data,
        run_tag, '-s 10 -c 1000', '-P .4', left_out))

    return results

def load(fname):
    return summary(cPickle.load(gzip.open(fname)))

def summary(data, metric=ft.partial(metrics.f1_score, average='macro')):
    results = np.array([[[metric(j[0], k)for k in j[1]] for j in i] for i in data])
    return results, np.vstack((results.mean(1), results.mean((0,1)), np.median(results, (0,1)), results.std((0,1)))), data

def main():
    cols = []

    for l in open('uci_har_data/features.txt'):
        if 'mean' in l:
            cols.append(int(l.strip().split()[0]) - 1)

    data = np.vstack((
        np.loadtxt('uci_har_data/train/X_train.txt', usecols=cols),
        np.loadtxt('uci_har_data/test/X_test.txt', usecols=cols)))

    data = preprocessing.MinMaxScaler().fit_transform(data)

    labels = np.hstack((
        np.loadtxt('uci_har_data/train/y_train.txt', int) - 1,
        np.loadtxt('uci_har_data/test/y_test.txt', int) - 1))

    results = util.DefaultList(list)

    for _ in xrange(40):
        for train, test in cv.KFold(len(labels), 5, shuffle=True):
            for left_out in set(labels[train]):
                index = labels[train] != left_out
                train_data = data[train][index]
                train_labels = labels[train][index]
                results[left_out].append((
                    labels[test],
                    compare_alternatives(
                        train_data, data[test], train_labels, left_out)))

    print summary(results)[1]

    try:
        if sys.argv[1] == 's':
            stamp = int(time.time())
            print stamp
            cPickle.dump(
                results, gzip.open('uci_har_results_%d.pickle.gz' % stamp, 'w'))
    except IndexError:
        pass

if __name__ == '__main__':
    signal.signal(signal.SIGINT, lambda *x: pdb.set_trace())

    np.set_printoptions(4, 2**100, linewidth=2**100, suppress=1)
    np.seterr('raise')

    main()
