import collections as cl
import functools as ft
import itertools as it

import numpy as np
import scipy.stats as st
import llist
import sklearn.metrics as metrics

import wann.encoding as encoding
import wann.util as util
import wann.wisard.discriminators as discriminators
import wann.wisard.neurons as neurons


class WiSARDLikeClassifier(object):
    '''The superclass of all WiSARD-like classifiers.

    This should be used as a template to any classifier implementation, as an
    abstract class, but no method is indeed required to be overriden.
    '''

    def __init__(self, *args, **kwargs):
        raise NotImplementedError('This class is abstract. Derive it.')

    def record(self, observation, class_):
        '''Record the provided observation, relating it to the given class.'''
        raise NotImplementedError('This method is abstract. Override it.')

    def answer(self, observation, class_=None):
        '''Returns how similar the observation is to the known classes.

        By default, a dictionary with the classes labels as keys and the
        respective similarities between observation and classes as values is
        returned.

        Parameters
            observation: the observation in which the answers must be based
            class_: if given, only the similarity wrt this class is returned
        '''

        raise NotImplementedError('This method is abstract. Override it.')

    def counts(self, observation, class_=None):
        '''Returns a description of observation similarity to known classes.

        The description of the similarity must take into account the number of
        times the observation addresses were previously recorded.

        By default, a dictionary with the classes labels as keys and theirs
        respective similarity descriptions as values is returned.

        Parameters
            observation: the observation in which the answers must be based
            class_: if given, only the answer wrt this class is returned
        '''

        raise NotImplementedError('This method is abstract. Override it.')

    def remove_class(self, class_):
        raise NotImplementedError('This method is abstract. Override it.')


class WiSARD(WiSARDLikeClassifier):
    def __init__(
            self,
            addresser=None,
            discriminator_factory=discriminators.FrequencyDiscriminator):
        '''Inits a WiSARD classifier using the provided arguments.

        Parameters
            discriminator: the discriminator which will be used to learn about
                each class to be presented. The argument must be callable,
                returning a Discriminator-like object.
        '''

        if addresser is None:
            self.addresser = encoding.BitStringEncoder()
        else:
            self.addresser = addresser
        self.discriminators = cl.defaultdict(discriminator_factory)

    def clear(self):
        self.discriminators.clear()

    def fit(self, observations, classes, clear=True):
        if clear:
            self.clear()

        data = it.izip(it.imap(self.addresser, observations), classes)

        for observation, class_ in data:
            self.discriminators[class_].record(observation)

        return self

    def answer(self, observation):
        return {class_: discriminator.answer(self.addresser(observation))
            for class_, discriminator in self.discriminators.viewitems()}

    def predict(self, observations):
        return [util.ranked(self.answer(observation))[0][0]
            for observation in observations]

    def bleach(self, threshold):
        for d in self.discriminators:
            self.discriminators[d].bleach(threshold)

    def remove_class(self, class_):
        del self.discriminators[class_]


class FrequencyMultiClassDiscriminator(WiSARD):
    def __init__(self, addresser=None, neuron=neurons.MultiValueNeuron):
        self.neuron_factory = neuron
        self.addresser = addresser or encoding.BitStringEncoder()
        self.clear()

    def clear(self):
        self.neurons = None
        self.nrecords = cl.defaultdict(int)

    def fit(self, observations, classes, clear=True):
        if clear:
            self.clear()

        for observation, class_ in it.izip(observations, classes):
            self.record(observation, class_)

        return self

    def record(self, observation, class_):
        observation = self.addresser(observation)

        if not self.neurons:
            self.neurons = [self.neuron_factory() for _ in observation]

        for address, neuron in it.izip(observation, self.neurons):
            neuron.record(address, class_)

        self.nrecords[class_] += 1

    def answer(self, observation, class_=None):
        observation = self.addresser(observation)
        if not self.neurons:
            self.neurons = [self.neuron_factory() for _ in observation]
        neurons_feedback = (neuron.count(address, class_)
            for address, neuron in it.izip(observation, self.neurons))

        if class_ is not None:
            return sorted(neurons_feedback)

        counter = cl.defaultdict(list)

        for freqs in neurons_feedback:
            for label, count in freqs.viewitems():
                counter[label].append(count)

        for f in counter.viewvalues():
            f.sort()

        return counter


class HitsMultiClassDiscriminator(FrequencyMultiClassDiscriminator):
    def answer(self, observation, class_=None):
        observation = self.addresser(observation)

        if not self.neurons:
            self.neurons = [self.neuron_factory() for _ in observation]

        neurons_feedback = (neuron.is_set(address, class_)
            for address, neuron in it.izip(observation, self.neurons))

        if class_ is not None:
            return sum(neurons_feedback)

        return cl.Counter(class_
            for classes in neurons_feedback for class_ in classes)


class OmniDiscriminator(object):
    def __init__(self, addresser=None, neuron_factory=neurons.ClassSetNeuron):
        if addresser is None:
            self.addresser = encoding.BitStringEncoder()
        else:
            self.addresser = addresser
        self.neurons = util.DefaultList(neuron_factory)
        self.classes = set()

    def clear(self):
        del self.neurons[:]
        self.classes.clear()

    def __len__(self):
        return len(self.neurons)

    def write(self, observation, class_):
        observation = self.addresser(observation)
        self.classes.add(class_)

        return [neuron.write(address, class_)
            for address, neuron in
                it.izip(observation, self.neurons.infinite_iter())]

    def read(self, observation, class_=None):
        observation = self.addresser(observation)

        reads = (neuron.read(address, class_)
            for address, neuron in
                it.izip(observation, self.neurons.infinite_iter()))

        if class_ is not None:
            return sum(reads) * 1. / len(self)

        #TODO: Consider using numpy.bincount to count frequencies faster

        ans = dict.fromkeys(self.classes, 0.)

        for read in reads:
            for scoring_class in read:
                ans[scoring_class] += 1.

        return {k: v/len(self) for k, v in ans.viewitems()}

    def fit(self, observations, classes, clear=True):
        if clear:
            self.clear()

        for observation, class_ in it.izip(observations, classes):
            self.write(observation, class_)

        return self

    def predict(self, observations):
        return [util.ranked(self.read(observation))[0][0]
            for observation in observations]


class RejectionOmniDiscriminator(OmniDiscriminator):
    def __init__(
            self, addresser=None, alpha=5, outlier_label=-1,
            neuron_factory=neurons.ExclusiveAddressNeuron):
        super(RejectionOmniDiscriminator, self).__init__(
            addresser, neuron_factory)
        self.alpha = alpha
        self.outlier_label = outlier_label

    def fit(self, observations, classes=None, clear=True):
        if classes is None:
            classes = [-self.outlier_label] * len(observations)

        super(RejectionOmniDiscriminator, self).fit(
            observations, classes, clear)

        if hasattr(self.alpha, '__call__'):
            self.optimize_rejection_thresholds(observations, classes)
        else:
            self.set_rejection_thresholds(classes)

        return self

    def get_leave_one_out_errors(self):
        return cl.Counter(it.chain.from_iterable(
            n.exclusive.values() for n in self.neurons))

    def set_rejection_thresholds(self, classes):
        loo_errors = self.get_leave_one_out_errors()

        self_matching_rates = {k: [] for k in self.classes}

        for i, class_ in enumerate(classes):
            self_matching_rates[class_].append(
                1. - loo_errors[i] * 1. / len(self))

        #import pdb; pdb.set_trace()

        self.rejection_thresholds = {
            k: np.percentile(self_matching_rates[k], self.alpha)
            #k: np.mean(self_matching_rates[k]) - np.std(self_matching_rates[k]) * self.alpha
                for k in self.classes}

    '''
    def optimize_rejection_thresholds(self, observations, classes):
        loo_errors = self.get_leave_one_out_errors()
        rates = cl.defaultdict(list)
        labels = cl.defaultdict(list)
        reads = map(self.read, observations)
        for i, (read, class_) in enumerate(zip(reads, classes)):
            read[class_] = 1. - loo_errors[i] * 1. / len(self)
            top_rank = util.ranked(read)[0]
            rates[top_rank[0]].append(top_rank[1])
            labels[top_rank[0]].append(top_rank[0] == class_)

        self.rejection_thresholds = {}

        for class_ in set(classes):
            class_rates = np.array(rates[class_])
            threshold_opts = np.unique(class_rates)
            best_opt = np.argmax([self.alpha(labels[class_], class_rates > t)
                for t in threshold_opts])
            self.rejection_thresholds[class_] = threshold_opts[best_opt]
    '''

    def optimize_rejection_thresholds(self, observations, classes):
        loo_errors = self.get_leave_one_out_errors()
        reads = map(self.read, observations)
        for i, (read, class_) in enumerate(zip(reads, classes)):
            read[class_] = 1. - loo_errors[i] * 1. / len(self)

        self.rejection_thresholds = {}

        for class_ in set(classes):
            class_rates = np.array([r[class_] for r in reads])
            threshold_opts = np.unique(class_rates)
            score = lambda t: self.alpha(classes == class_, class_rates > t)
            best_opt = np.argmax([score(t) for t in threshold_opts])

            self.rejection_thresholds[class_] = threshold_opts[best_opt]

    def predict(self, observations):
        predictions = []
        for observation in observations:
            class_, score = util.ranked(self.read(observation))[0]
            if score > self.rejection_thresholds[class_]:
                predictions.append(class_)
            else:
                predictions.append(self.outlier_label)

        return predictions


class TemporalOmniDiscriminator(OmniDiscriminator):
    def __init__(self, addresser=None, neuron_factory=neurons.ClassSetNeuron):
        super(TemporalOmniDiscriminator, self).__init__(
            addresser, neuron_factory)
        self.history = cl.OrderedDict()
        self.class_addresses_cnt = cl.defaultdict(int)

    def write(self, temporal_observation, class_):
        observation, timestamp = temporal_observation
        changes = super(TemporalOmniDiscriminator, self).write(
            observation, class_)

        for i, address in enumerate(self.addresser(observation)):
            if changes[i]:
                self.class_addresses_cnt[class_] += 1
            else:
                del self.history[i, address, class_]

            self.history[i, address, class_] = timestamp

        return changes

    def bleach(self, min_timestamp):
        changes = []
        deleted_classes = []

        for key in self.history:  # key = neuron_index, address, class_
            if self.history[key] < min_timestamp:
                self.neurons[key[0]].erase(*key[1:])
                del self.history[key]

                if self.class_addresses_cnt[key[-1]] == 1:
                    deleted_classes.append(key[-1])
                    del self.class_addresses_cnt[key[-1]]
                    self.classes.remove(key[-1])
                else:
                    changes.append(key)
                    self.class_addresses_cnt[key[-1]] -= 1

            else:
                break

        return changes, deleted_classes


class CoverageOmniDiscriminator(OmniDiscriminator):
    def __init__(self, addresser=None, neuron_factory=neurons.ClassSetNeuron):
        super(CoverageOmniDiscriminator, self).__init__(
            addresser, neuron_factory)
        self.coverage = {}
        self.lengths = cl.defaultdict(lambda: np.zeros(len(self), dtype=int))

    def write(self, observation, class_):
        changes = super(CoverageOmniDiscriminator, self).write(
            observation, class_)
        self.lengths[class_] += changes
        self.coverage[class_] = None

        return changes

    def read(self, observation, class_=None):
        answer = super(CoverageOmniDiscriminator, self).read(
            observation, class_)

        if class_ is not None:
            return answer / self._class_coverage(class_)

        return {class_: score/self._class_coverage(class_)
            for class_, score in answer.iteritems()}

    def _class_coverage(self, class_):
        if not self.coverage[class_]:
            self.coverage[class_] = st.gmean(self.lengths[class_])
        return self.coverage[class_]


class TemporalCoverageOmniDiscriminator(
        TemporalOmniDiscriminator, CoverageOmniDiscriminator):
    def bleach(self, min_timestamp):
        changes, deleted_classes = super(
            TemporalCoverageOmniDiscriminator, self).bleach(min_timestamp)

        for neuron, _, class_ in changes:
            self.lengths[class_][neuron] -= 1
            self.coverage[class_] = None

        for class_ in deleted_classes:
            del self.coverage[class_]
            del self.lengths[class_]

        return changes, deleted_classes


class SelfContainedWiSARD(WiSARDLikeClassifier):
    def __init__(self, nmbr_neurons):
        discriminator = lambda: [
            cl.defaultdict(int) for _ in it.repeat(None, nmbr_neurons)]

        self.dscrmntrs = cl.defaultdict(discriminator)

    def record(self, observation, class_):
        for address, neuron in it.izip(observation, self.dscrmntrs[class_]):
            neuron[address] += 1

    def answer(self, observation, is_sorted=False, class_=None,
            normalized=False):
        if class_ is not None:
            answer = sum(address in neuron for address, neuron in
                it.izip(observation, self.dscrmntrs[class_]))

            if normalized:
                return answer * 1. / len(self.dscrmntrs[class_])

            return answer

        raise Exception


    def bleach(self, threshold):
        for d in self.dscrmntrs.values():
            for neuron in d:
                for address, value in neuron.items():
                    if value <= threshold:
                        del neuron[address]
                    else:
                        neuron[address] -= threshold


def ClusWiSARDfactory(parent=WiSARD):
    return type('ClusWiSARDsubclass', (ClusWiSARD, parent), {})


class ClusWiSARD(WiSARDLikeClassifier):
    def __getstate__(self):
        global ClusWiSARDsubclass
        ClusWiSARDsubclass = self.__class__
        dict_ = self.__dict__.copy()
        dict_['clusters'] = list(dict_['clusters'])
        return dict_

    def __setstate__(self, dict_):
        dict_['clusters'] = llist.dllist(dict_['clusters'])
        self.__class__ = ClusWiSARDfactory()
        self.__dict__.update(dict_)

    def __init__(self, min_similarity, expected_absorptions, max_clusters=None,
            *args, **kwargs):
        super(ClusWiSARD, self).__init__(*args, **kwargs)
        self.min_similarity = min_similarity
        self.expected_absorptions = expected_absorptions
        self.max_clusters = max_clusters
        self.clear()

    def clear(self):
        self.clusters = llist.dllist()
        self.next_id = 0
        self.infos = {}
        self.discriminators.clear()

    def fit(self, observations, classes):
        for ax in zip(observations, classes):
            self.record(*ax)

    def record(self, observation, class_=None):
        for node, cluster in util.iter_llist_items(self.clusters):
            info = self.infos[cluster]

            if info.class_ is None or class_ is None or info.class_ == class_:
                threshold = info.absorptions * 1.
                threshold /= self.expected_absorptions
                threshold += self.min_similarity

                answer = self.hits(observation, cluster)
                if answer >= len(self.discriminators.values()[0]) * min(threshold, 1.):
                    self.clusters.remove(node)
                    break
        else:
            if len(self.infos) == self.max_clusters:
                to_remove = min(
                    (v.absorptions, k) for k, v in self.infos.viewitems())
                self.remove_class(to_remove[1])

            cluster, self.next_id = self.next_id, self.next_id + 1
            info = self.infos[cluster] = util.ClusterInfo()

        if class_ is not None:
            info.class_ = class_

        super(ClusWiSARD, self).fit([observation], [cluster])
        self.clusters.appendleft(cluster)
        info.absorptions += 1

        return cluster

    def remove_class(self, cluster):
        try:
            node, cluster = cluster, cluster.value
            return_value = node.next
        except AttributeError:
            node = self.clusters.first
            while node:
                if node.value == cluster:
                    break
                node = node.next

            return_value = None

        super(ClusWiSARD, self).remove_class(cluster)
        self.clusters.remove(node)
        del self.infos[cluster]

        return return_value

    def discard(self, threshold):
        node = self.clusters.first
        while node:
            if self.infos[node.value].absorptions <= threshold:
                node = self.remove_class(node)
            else:
                node = node.next


class ClusWiSARDsubclass(ClusWiSARD):
    pass
