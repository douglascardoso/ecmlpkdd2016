import pdb
import sys

import numpy as np


def _answers_sortkey(item):
    try:
        return len(item[1]), item[1], np.random.random()
    except TypeError:
        return item[1], np.random.random()


def ranked(dict_, key_func=_answers_sortkey):
    """Returns a list of the items in dict_ sorted by key_func."""
    return sorted(dict_.viewitems(), key=key_func, reverse=1)


def iter_llist_items(llist):
    """Yields a tuple (node, node.value) for each node in llist"""
    node = llist.first
    while node:
        yield node, node.value
        node = node.next


class ClusterInfo(object):
    """The container of all information of a cluster."""
    def __init__(self):
        self.absorptions = 0
        self.class_ = None

    def __repr__(self):
        return str((self.absorptions, self.class_))


def disjunct_bitstrings(total_bits, repeat_bits=None):
    """Yields bit strings as Hamming-disjunct as possible.

    Yields bit strings of length total_bits which are as far as
    possible (wrt Hamming distance) from previously yielded strings.

    If repeat_bits is None, all 2**total_bits strings between 0 and
    2**total_bits - 1 will be yielded in some order. Else,
    2**repeat_bits strings will be yielded.

    The strings are ordered in a brute-force fashion. So, the use this
    function for arbitary values is time-prohibitive: total_bits, or
    repeat_bits when in use, should be smaller than 64.
    """
    if repeat_bits is not None:
        repetitions = int(np.ceil(total_bits * 1. / repeat_bits))

        for mask in disjunct_bitstrings(repeat_bits):
            yield (mask*repetitions)[:total_bits]

        return

    #TODO: make a better, generic, cross-plataform check for overflow
    if total_bits > 63:
        raise OverflowError(
            'Please lower the requested number of bits: %d' % total_bits)

    yield '0'*total_bits
    yield '1'*total_bits

    olds = [0, 2**total_bits - 1]

    for distance in xrange(total_bits/2, 0, -1):
        for value in xrange(1, 2**total_bits-1):
            min_distance = min(bin(value ^ old).count('1') for old in olds)

            if min_distance == distance:
                olds.append(value)
                yield bin(value)[2:].zfill(total_bits)


def iterator2factory(iterator):
    """Returns a function which returns next(iterator) at each call."""
    return lambda: next(iterator)


def iterative_bleaching(counts, big_steps=False):
    #TODO: Complete review
    bleach_level = 0

    while 1:
        if big_steps:
            try:
                min_count = max(counts.viewitems(), key=_answers_sortkey)[1][0]
            except IndexError:
                return

        else:
            try:
                min_count = min(v[0] for v in counts.values() if v)
            except ValueError:
                return

        yield counts, bleach_level, min_count

        bleach_level += min_count

        for class_, class_counts in counts.viewitems():
            counts[class_] = [
                cc-min_count for cc in class_counts if cc > min_count]


class DefaultList(list):
    def __init__(self, default, *args, **kwargs):
        self._default = default
        super(DefaultList, self).__init__(*args, **kwargs)

    def __getitem__(self, key):
        try:
            return super(DefaultList, self).__getitem__(key)
        except:
            self.extend(self._default() for _ in xrange(key - len(self) + 1))
            return super(DefaultList, self).__getitem__(key)

    def __setitem__(self, key, value):
        try:
            return super(DefaultList, self).__setitem__(key, value)
        except:
            self.extend(self._default() for _ in xrange(key - len(self) + 1))
            return super(DefaultList, self).__setitem__(key, value)

    def infinite_iter(self):
        for item in super(DefaultList, self).__iter__():
            yield item

        while 1:
            self.append(self._default())
            yield self[-1]

    def get(self, key, default):
        try:
            return super(DefaultList, self).__getitem__(key)
        except:
            return default


class ForkedPdb(pdb.Pdb):
    """A Pdb subclass that may be used from a forked multiprocessing child"""

    def interaction(self, *args, **kwargs):
        _stdin = sys.stdin
        try:
            sys.stdin = file('/dev/stdin')
            pdb.Pdb.interaction(self, *args, **kwargs)
        finally:
            sys.stdin = _stdin


def stack_histogram(data):
    histogram = np.histogram(data)
    return np.vstack((np.hstack((histogram[0], [-1])), histogram[1]))


def median_and_mad(values):
    median = np.median(values)
    return median, np.median(np.abs([v - median for v in values]))
