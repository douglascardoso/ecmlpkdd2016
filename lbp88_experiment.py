#BitStringEncoder: bit_pool, hstack
import cPickle
import functools as ft
import gzip
import itertools as it
import os
import pdb
import signal
import sys
import time

import numpy as np
np.set_printoptions(4, linewidth=9999, suppress=1)
import sklearn.datasets as ds
import sklearn.metrics as metrics
import sklearn.naive_bayes as nb
import sklearn.neighbors as neighbors
import sklearn.preprocessing as preprocessing
import sklearn.svm as svm

import wann.encoding as encoding
import wann.wisard.classifiers as wis_clf
import wann.util as util

import my_libsvm

run_tag = 'lbp88_%d'%time.time()

fbeta = lambda x: ft.partial(metrics.fbeta_score, beta=x)

def compare_alternatives(train_data, test_data, train_labels):
    ss = preprocessing.StandardScaler()

    addresser = [encoding.EncodingComposer(
        encoding.UnaryEncoder(1000), encoding.BitStringEncoder(100, mode=1))]

    train_data = [train_data.todense().getA()]
    test_data = [test_data.todense().getA()]

    train_data.append(ss.fit_transform(train_data[0]))
    test_data.append(ss.transform(test_data[0]))

    train_data.append(np.clip((train_data[1] + 5.)/10., 0, 1))
    test_data.append(np.clip((test_data[1] + 5.)/10., 0, 1))

    clf_alternatives = [
        (svm.SVC(gamma=35), 0),
        (neighbors.KNeighborsClassifier(), 0),
        (nb.GaussianNB(), 0),
        (wis_clf.OmniDiscriminator(addresser[0]), 2),
        (wis_clf.RejectionOmniDiscriminator(addresser[0], fbeta(.4)), 2),
        (wis_clf.RejectionOmniDiscriminator(addresser[0], 50), 2),
        ]

    predictions = []

    for clf_i, (clf, data_i) in enumerate(clf_alternatives):
        clf.fit(train_data[data_i], train_labels)
        predictions.append(clf.predict(test_data[data_i]))

    predictions.append(svm.OneClassSVM(gamma=35).fit(train_data[0][train_labels == 1.]).predict(test_data[0]))

    predictions.append(my_libsvm.do_svm(
        train_data[0], train_labels, test_data[0],
        run_tag, '-s 10 -g 35 -N', '-P .5'))

    return predictions

def load(fname):
    return summary(cPickle.load(gzip.open(fname)))

def summary(results):
    f1_scores = np.array([[[metrics.precision_recall_fscore_support(b[0], c) for c in b[1]] for b in a] for a in results])
    return f1_scores, np.vstack((f1_scores.mean((0, 1)), f1_scores.std((0, 1))))

def main():
    results = util.DefaultList(ft.partial(util.DefaultList, list))

    for xp_run in range(ord('e') - ord('a') + 1):
        basedir = (
            'lbp88_data/grab-feature-vectors-subset-88-' +
            chr(xp_run + ord('a')) + '/')

        train_list = open(basedir + 'twoclass-bal-training.list')
        test_list = open(basedir + 'testing.list')

        for files in zip(train_list, test_list):
            #print files
            data = ds.load_svmlight_files([basedir + f.strip() for f in files])
            train_data, train_labels, test_data, test_labels = data

            predictions = compare_alternatives(
                train_data, test_data, train_labels)

            results[xp_run].append((test_labels, predictions))

    print summary(results)[1]

    try:
        if sys.argv[1] == 's':
            stamp = int(time.time())
            print stamp
            cPickle.dump(
                results, gzip.open('lbp88_results_%d.pickle.gz' % stamp, 'w'))
    except IndexError:
        pass

if __name__ == '__main__':
    signal.signal(signal.SIGINT, lambda *x: pdb.set_trace())

    np.set_printoptions(4, 2**100, linewidth=2**100, suppress=1)
    np.seterr('raise')

    main()
