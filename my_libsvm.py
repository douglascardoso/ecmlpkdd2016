import sys
import re
import subprocess as sp
import sklearn.datasets as ds

def do_svm(
        train_data, train_labels, test_data,
        run_tag, train_args='', predict_args='', outlier_label=-1):
    ds.dump_svmlight_file(
        train_data, train_labels, '/tmp/%s_libsvm_train'%run_tag)
    cmd = (
        'libsvm-openset/svm-train -q ' +
        '%s /tmp/%s_libsvm_train /tmp/%s_libsvm_model')
    cmd = cmd%(train_args, run_tag, run_tag)
    if sp.call(cmd.split(), stdout=sp.PIPE, stderr=sp.PIPE):
        raise Exception
    ds.dump_svmlight_file(
        test_data, [0] * len(test_data), '/tmp/%s_libsvm_test'%run_tag)
    cmd = (
        'libsvm-openset/svm-predict %s ' +
        '/tmp/%s_libsvm_test /tmp/%s_libsvm_model /tmp/%s_libsvm_out')
    cmd = cmd%(predict_args, run_tag, run_tag, run_tag)
    if sp.call(cmd.split(), stdout=sp.PIPE, stderr=sp.PIPE):
        raise Exception
    libsvm_out = open('/tmp/%s_libsvm_out'%run_tag).read()
    libsvm_out = libsvm_out.replace('-99999', str(outlier_label))
    return map(int, re.findall('-?\d', libsvm_out))
