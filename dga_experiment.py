#BitStringEncoder: bit_pool, vstack
import cPickle
import collections as cl
import functools as ft
import gzip
import itertools as it
import pdb
import signal
import sys
import time

import numpy as np
np.set_printoptions(4, linewidth=9999, suppress=1)
import scipy.io as io
import scipy.stats as st
import sklearn.cluster as cluster
import sklearn.cross_validation as cv
import sklearn.grid_search as gs
import sklearn.metrics as metrics
import sklearn.naive_bayes as nb
import sklearn.neighbors as neighbors
import sklearn.preprocessing as preprocessing
import sklearn.svm as svm

import wann.encoding as encoding
import wann.wisard.classifiers as wis_clf
import wann.util as util

import my_libsvm

nclusters = 10

run_tag = 'dga_%d'%time.time()

def compare_alternatives(train_data, test_data, train_labels):
    results = []
    addresser = [encoding.EncodingComposer(
        encoding.UnaryEncoder(100), encoding.BitStringEncoder(10, 100, mode=2))]
    addresser.append(encoding.EncodingComposer(
        encoding.UnaryEncoder(100), encoding.BitStringEncoder(100, 100, mode=2)))

    clf_alternatives = [
        svm.SVC(C=10, gamma=10),
        neighbors.KNeighborsClassifier(),
        nb.GaussianNB(),
        wis_clf.OmniDiscriminator(addresser[0]),
        wis_clf.RejectionOmniDiscriminator(addresser[1], metrics.f1_score, 1),
        ]

    for clf in clf_alternatives:
        clf.fit(train_data, train_labels)
        results.append(clf.predict(test_data))

    dtc_alternatives = [
        wis_clf.RejectionOmniDiscriminator(addresser[1], outlier_label=1),
        svm.OneClassSVM(nu=.005, gamma=.025),
        ]

    for dtc in dtc_alternatives:
        dtc.fit(train_data[train_labels == -1])
        results.append(dtc.predict(test_data))

    results.append(my_libsvm.do_svm(
        train_data, train_labels, test_data,
        run_tag, '-s 10 -c 10 -g 10 -N'))

    return results

def summary(results):
    f1_scores = [np.array([[metrics.f1_score(test[0], t) for t in test[1]] for test in r]) for r in results]
    means = np.vstack([a.mean(0) for a in f1_scores])
    return f1_scores, np.vstack((means, np.median(means, 0), means.mean(0), means.std(0)))

def load(fname):
    return summary(cPickle.load(gzip.open(fname)))

def main():
    data = io.loadmat('dga_data')
    data, labels = data['dgoa'].T, data['labels'][0].astype(int)
    labels[labels == 0] = -1
    data = preprocessing.MinMaxScaler().fit_transform(data)
    results = util.DefaultList(list)

    for _ in xrange(1000):
        for train, test in cv.KFold(len(labels), 5, shuffle=True):
            train_data = data[train]
            test_data = data[test]
            results[0].append((
                labels[test],
                compare_alternatives(data[train], data[test], labels[train])))

    clusterer = cluster.KMeans(nclusters, init='random')
    pos_data = data[labels == 1]
    neg_data_it = cv.ShuffleSplit((labels == -1).sum(), 10**6, .2)

    for _ in xrange(100):
        clusters = clusterer.fit_predict(pos_data)

        for ntc_i, n_train_clusters in enumerate(xrange(2, nclusters, 3), 1):
            for train_clusters in it.combinations(
                    range(nclusters), n_train_clusters):
                train_data, test_data = [], []
                train_labels, test_labels = [], []
                for observation, cluster_id in zip(pos_data, clusters):
                    if cluster_id in train_clusters:
                        train_data.append(observation)
                        train_labels.append(1)
                    else:
                        test_data.append(observation)
                        test_labels.append(1)

                neg_data = next(iter(neg_data_it))
                train_data = np.vstack((
                    train_data, data[labels == -1][neg_data[0]]))
                test_data = np.vstack((
                    test_data, data[labels == -1][neg_data[1]]))
                train_labels = np.hstack((
                    train_labels, [-1] * len(neg_data[0])))
                test_labels = np.hstack((
                    test_labels, [-1] * len(neg_data[1])))

                results[ntc_i].append((
                    test_labels,
                    compare_alternatives(
                        train_data, test_data, train_labels)))

    print summary(results)[1]

    try:
        if sys.argv[1] == 's':
            stamp = int(time.time())
            print stamp
            cPickle.dump(
                results, gzip.open('dga_results_%d.pickle.gz' % stamp, 'w'))
    except IndexError:
        pass

if __name__ == '__main__':
    signal.signal(signal.SIGINT, lambda *x: pdb.set_trace())

    np.set_printoptions(4, 2**100, linewidth=2**100, suppress=1)
    np.seterr('raise')

    main()
